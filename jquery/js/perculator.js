
$(document).ready(function(){
    $('#pages').perculator();
});
(function($) {
    $.fn.perculator = function(options) {
        var img_fondo = new Array();
        var  plugin_page  = $(this);
        var settings = $.extend({
            //These are the defaults
            vertical_position: 65, //From bottom to the top div of the plugin
            page_height:360,
            page_width: 800, //If width it's lesser than viewport with this parameter should be discarted
            plugin_style: 'default',
            slide_offset:50
        }, options);
        //crear capa wrapper dinamicament amb 10000px
        var contenido = $(this).html();

        $(this).html('');
        $(this).prepend('<div class="wrapper" style="{width:10000px}">' + contenido +'</div>');

        $(this).css({
            'position': 'absolute',
            'width': 100+'%',
            'height': settings.page_height,
            'bottom': settings.vertical_position,
            'overflow': 'auto',
            'overflow-x': ' hidden',
            'overflow-y': 'hidden '


        });
        $(this).find('.text_page').css({

            'width': settings.page_width,
            'max-width': '800px',//calculate from the viewport
            'height': settings.page_height
           // 'bottom': settings.vertical_position,

        });

        var init_pos=$(document).outerWidth()*3/4; // medix el viewport

        $('#full-screen').click(function() {
            if ($("#pages").is(":hidden")) {
                $("#title").show().animate({left: "50%"}, 700);
                $("#pages").css("left", "");
                $("#pages").show();
                $("#pages").animate({
                    scrollLeft: pos
                }, 1500);
            } else {
                $("#title").animate({left: "-200%"}, 700, function () {
                    $(this).hide();
                });
                $("#pages").animate({left: "200%"}, 700, function () {
                    $(this).hide();
                });
                //$("#pages").hide();
            }
        })


            var   prev_layer_pos =0;

        $(this).find('img.item_page_background').css({'visibility': 'hidden','display':'none'});


        $(this).find('.text_page').each(function(index) {





            $(this).find('img.item_page_background').css({'visibility': 'hidden', 'display': 'none'});
          var  imagen_page_fondo = $(this).find('img.item_page_background').attr('src');
           img_fondo.push(imagen_page_fondo);
            $(this).find('div.title').css({'visibility': 'hidden', 'display': 'none'});
           //titulo = $(this).find('div.title').html();




            var thiz = $(this); //text_page class
            thiz.css({'background-img': imagen_page_fondo});
            if (index == 0) {

                thiz.css("left", init_pos + "px");
                prev_layer_pos += init_pos + settings.page_width + settings.slide_offset;
             //   console.log(prev_layer_pos);
            } else {
              //  console.log(prev_layer_pos + settings.slide_offset + "px");
                thiz.css({"left": prev_layer_pos + "px"});
                prev_layer_pos += settings.page_width + settings.slide_offset;

            }

            var estil = "";
            if (index == 0) {
                estil = "left:50%;";
            } else {
                estil = "left:0%;";
            }

            if (index === 0) prefix_str = ""; else prefix_str = "_next";


            var new_item_selector = "<a href='#'> <div id='" + index + "' class='item' style='position:absolute;top:50%;" + estil + "margin-left:-42px;margin-top:-42px;width:84px;height:84px;background-image: url(img/buttons/nr" + (index + 1) + prefix_str + ".png);' ></div> </a>";
            //var new_photo_colorbox = "<a href='#'><div id='img_" + index +"' data-image='"+imagen_page_fondo+"' data-gallery='gallery2' data-desc='"+footer+"'"+ "' class='img_colorbox' style='position:absolute;top:41%;left:71%;width:65px;height:65px;background-image: url();' /></a>";
            var new_photo_colorbox="<a href='#'><div id='img_"+index+"' data-image='"+imagen_page_fondo+"' class='img_colorbox' style='position:absolute;top:50%;left:75%;margin-left:-150px;margin-top:-130px;width:250px;height:217px;background-image: url(img/buttons/button_colorbox.png);' /></a>";

            thiz.append(new_item_selector);
            thiz.append(new_photo_colorbox)
        })

        //'<div style="{width:' + settings.page_width + ',height:' + settings.page_height + ',bottom:0px}"></div>
        $(this).find('.wrapper').append('<div style="width:' + settings.page_width+'px' + ';height:' + settings.page_height+'px' + ';position:absolute;left:' + prev_layer_pos + 'px;"></div>');
        $(".footer_text").each(function(index, value) {
            $(this).css("margin-left", -$(this).outerWidth() / 2);
        });
            var first_elem_pos=init_pos-$(document).outerWidth()/2+400;
            var pos = first_elem_pos;
        $.vegas({
            src: img_fondo[0]
          //  fade:1500
        });
            $( ".item" ).click(function() {

                var id = $(this).attr('id');
                $(this).css("background-image","url(img/buttons/nr"+(parseInt(id)+1)+".png)");
                $(this).animate({left:'50%'},1250);

                $(".item").each(function(index,value){
                    var thiz=$(this);
                    if (index<parseInt(id)){
                        thiz.animate({left:'100%'},1250);
                        thiz.css("background-image","url(img/buttons/nr"+(index+1)+"_back.png)");
                        $.vegas({
                            src: img_fondo[index + 1]
                         //   fade:1500
                        });
                    }else if(index>parseInt(id)){
                        thiz.animate({left:'0%'},1250);
                        thiz.css("background-image","url(img/buttons/nr"+(index+1)+"_next.png)");
                    }
                });

                if (id!=="0") {
                    pos= first_elem_pos+(settings.page_width*(parseInt(id))+settings.slide_offset*(parseInt(id)));
                }else{
                    $.vegas({
                        src: img_fondo[0]
                     //   fade:1500
                    });
                    pos = first_elem_pos;
                }
                plugin_page.animate({

                    scrollLeft:pos

                },1250);
                $('.img_colorbox').click(function(){

                });
            });

            //Fourth step
            $(this).animate({
                scrollLeft:init_pos-($(document).outerWidth()/2)+(settings.page_width/2)
            },1250);


            /*return this.each(function() {
             // Do something to each element here.
             });*/
        return this;

        };









}(jQuery));



