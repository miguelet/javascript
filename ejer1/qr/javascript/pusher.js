// Enable pusher logging - don't include this in production



function pusher(){
    ws = new WebSocket("ws://miguelet.ovh:9999");
    ws.onopen = function ()
    {
        // Web Socket is connected, send data using send()
       
    };
    ws.onmessage = function (evt)
    {
        var received_msg = evt.data; 
        document.getElementById("campo_isb").value = received_msg;
        libro(document.getElementById("campo_isb").value);
    };
    ws.onclose = function (){
   
    };
}


function libro(url){
    var obj;
    var xmlhttp;
    if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    }else{// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function (){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
            obj = JSON.parse(xmlhttp.responseText);
            var book1 = new book(obj.language, obj.isbn, obj.author, obj.title, obj.subtitle, obj.edition, obj.pub_date, obj.pub_place, obj.publisher, obj.extent, obj.editor);
            console.log(book1)
            plena(book1);
        }
    }
    xmlhttp.open("GET", "http://pere.bocairent.net/z39.php?isbn=" + url, false);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    xmlhttp.send(null);
}
function book(language, isbn, author, title, subtitle, edition, pub_date, pub_place, publisher, extent, editor) {
    this.language = language;
    this.isbn = isbn;
    this.author = author;
    this.title = title;
    this.subtitle = subtitle;
    this.edition = edition;
    this.pub_date = pub_date;
    this.pub_place = pub_place;
    this.publisher = publisher;
    this.extent = extent;
    this.editor = editor;
}
function plena(book1) {
   var ejemplo = document.getElementById('ejemplo');
    ejemplo.parentNode.removeChild(ejemplo);
    var conect = document.getElementById('conectado');
    conect.style.display = 'block';
    var x;
    for (x in book1) {
        var j = document.createElement("input");
        j.setAttribute("value", book1[x]);
        var k = document.createElement("label");
        k.innerHTML = x;
      var  b = document.createElement('br');
        document.getElementById("conectado").appendChild(k);
        document.getElementById("conectado").appendChild(j);
        document.getElementById("conectado").appendChild(b);
        //document.getElementById("conectado").appendChild(a);
        

    }
    new QRCode(document.getElementById("qrcode"), "http://miguelet.ovh/pere/server/www/reader.php?isbn=" + book1.isbn);

}
function enlazar() {
    var isbn = document.getElementById("campo_isb").value;
    if (is_valid_isbn(isbn)) {
        var txt = document.getElementById("campo_isb").value.replace(/-/g, "");
        if (txt.length == 10 || txt.length == 13) {
            if (valido_existente_isbn(txt)) {
                document.getElementById("buscar").disabled = false;
                document.getElementById("error").innerHTML = "";
            } else {
                document.getElementById("error").innerHTML = "mal isb";
            }
        } else {
            document.getElementById("error").innerHTML = "mal isb";
        }


    } else {

    }
}
function al_libro() {
    var isbn = document.getElementById("campo_isb").value;
    libro(isbn);

}

function is_valid_isbn(isb) {
    var valida_isb = /(?=.{13}$)\d{1,5}([- ])\d{1,7}\1\d{1,6}\1(\d|X)$/i;
    return valida_isb.test(isb);

}

function valido_existente_isbn(isbn) {
    if (isbn.length == 10) {
        var acum = 0;
        for (var i = 1; i < isbn.length; i++) {
            acum += i * isbn[i - 1];
        }
        if (acum % 11 < 10 && acum % 11 == isbn[isbn.length - 1]) {
            return true;
        } else if (acum % 11 == 10 && "X" == isbn[isbn.length - 1].toUpperCase()) {
            return true;
        } else {
            return false;
        }
    } else if (isbn.length == 13) {
        var acum = 0;
        var initial = 1;
        for (var i = 1; i < isbn.length; i++) {
            acum += initial * isbn[i - 1];
            initial == 1 ? initial = 3 : initial = 1;
        }
        if ((acum % 10 == 0 && isbn[isbn.length - 1] == 0) || (10 - acum % 10) == isbn[isbn.length - 1]) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}