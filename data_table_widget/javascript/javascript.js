
//constructor
function Data_Table(tabla, paginador, from, longitud,
        div_combobox, selCombo, url, num, btn_anterior, btn_siguiente, txt_buscar) {

    /*
     * 
     * @type Data_Table 
     * Contructor del Objecte
     */

    var this_ = this;
    this.tabla = tabla;
    this.paginador = paginador;
    this.from = from;
    this.longitud = longitud;
    this.div_combobox = div_combobox;
    this.selCombo = selCombo;
    this.url = url;
    this.num = num;
   this.txt_buscar = txt_buscar;
    //parametres per a url
    this.sSearch="&sSearch=";  this.buscar='';
    this.iDisplayStart = "iDisplayStart="
    this.iDisplayLength = "&iDisplayLength="

    this.iSortCol_="&iSortCol_"; this.iSortCol_NUM=0; this.iSortCol_value=0;
    this.sSortDir="&sSortDir=";  this.sSortDir_value="asc";
    this.iSortingCols="&iSortingCols="; this.iSortingCols_value="0";
    this.bSortable_="&bSortable_"; this.bSortable_NUM=0; this.bSortable_value="false";

    // auxliars
    this.aux_long = 0;
    this.auxmenos = 0;
     this.aux = 0;



    //   document.getElementById(div_combobox).onchange = function () {
    //cambiar(tabla,selCombo);
    // };

    /*
     * 
     * @param {type} from
     * @param {type} longitud
     * @returns {undefined} cridara a la funcion Json
     */


    this.init = function (from, longitud) {
        var aon = this_.url + this_.iDisplayStart + from + this_.iDisplayLength + longitud+this_.sSearch+this_.buscar+
                this_.iSortCol_+this_.iSortCol_NUM+"="+this_.iSortCol_value+this_.sSortDir+this_.sSortDir_value+this_.iSortingCols
        +this_.iSortingCols_value+this_.bSortable_+this_.bSortable_NUM+"="+this_.bSortable_value;
       console.log(aon);
        getDataTable().get_Json_data(aon,from,longitud);
    };
    /*
     * 
     * @param {type} from on acomensara
     * @param {type} on obtindrem el  jSon
     * @param {type} longitud cuants valors en traura
     * @returns ens torna un Json 
     * cridarem a la funcion Crear_tabla per a que ens cree la tabla apartir del Json
     * 
     */
    this.get_Json_data = function (url,from,longitud) {
        var datos;
        var xmlhttp;
        var i=parseInt(from)+parseInt(longitud);
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        ;
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                datos = JSON.parse(xmlhttp.responseText);
                document.getElementById(paginador).innerHTML = "de " + datos.aaData[0][0] + " a " + i  + " de " + datos.iTotalRecords + " filas ";



                getDataTable().Crear_tabla(datos, tabla);

            }
            ;
        };
        xmlhttp.open("GET", url, false);
        xmlhttp.send(null);

    };
    /*
     * Metode per a crar tabla dinamica amb el contingut del json
     */
    this.Crear_tabla = function (datos, tabla) {
        var tbody = document.getElementById(tabla);
        var bucle;
        for (bucle in  datos.aaData) {

            var row = document.createElement("TR");
            var td1 = document.createElement("TD");
            var td2 = document.createElement("TD");
            var td3 = document.createElement("TD");
            var td4 = document.createElement("TD");
            td1.appendChild(document.createTextNode(datos.aaData[bucle][0]));
            td2.appendChild(document.createTextNode(datos.aaData[bucle][1]));
            td3.appendChild(document.createTextNode(datos.aaData[bucle][2]));
            td4.appendChild(document.createTextNode(datos.aaData[bucle][3]));
            row.appendChild(td1);
            row.appendChild(td2);
            row.appendChild(td3);
            row.appendChild(td4);
            tbody.appendChild(row);
        }
        ;
    };
    /*
     * Funcio per eliminar les tuples que em creeat dinamicament
     */
    this.deleteRow = function () {
        try {
            var table = document.getElementById(this_.tabla);
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                table.deleteRow(i);
                rowCount--;
                i--;
            }
        } catch (e) {
            alert(e);
        }

    };


    this.cambiar = function (selCombo) {
        getDataTable().deleteRow(this_.tabla);
        if (document.getElementById(selCombo).selectedIndex === 0) {
            this_.longitud = 10;
            this_.aux_long = 0;
            this_.from = 0;
        } else if (document.getElementById(selCombo).selectedIndex === 1) {
            this_.longitud = 50;
            this_.aux_long = 0;
            this_.from = 0;
        } else if (document.getElementById(selCombo).selectedIndex === 2) {
            this_.longitud = 100;
            this_.aux_long = 0;
            this_.from = 0;
        } else {
            this_.longitud = 1000;
            this_.aux_long = 0;
            this_.from = 0;
        }

        getDataTable().init(this_.from, this_.longitud);

    };


document.getElementById(txt_buscar).onblour=function(){

   this_.buscar =  document.getElementById(txt_buscar).value;
   getDataTable().init(this_.aux_long, this_.from);

}

/*
 * botons per a paginar la tabla arrere i el de despres avan
 */
    document.getElementById(btn_anterior).addEventListener("click", function () {

        if ((this_.aux_long) === 0) {


        } else {
            getDataTable().deleteRow(this_.tabla);
            this_.from = parseInt(this_.longitud) - parseInt(this_.aux);


            this_.aux_long = parseInt(this_.aux_long) - parseInt(this_.longitud);


            getDataTable().init(this_.aux_long, this_.from);
        }
    });


    document.getElementById(btn_siguiente).addEventListener("click", function () {
        getDataTable().deleteRow(this_.tabla);
        this_.from = parseInt(this_.longitud) + parseInt(this_.aux);
        this_.aux_long = parseInt(this_.aux_long) + parseInt(this_.longitud);

        getDataTable().init(this_.aux_long, this_.from);

    });
/*
 * combobox
 */
    document.getElementById(div_combobox).onchange = function () {
        getDataTable().cambiar(selCombo);
    };

}
;

var getDataTable = (function () {
    var datatable = new Data_Table("tabla", "paginador", "0", "10", "combobox", "selCombo", "http://www.miguelet.ovh/pere/data_table_widget/server/server_processing2.php?", "0", "btn_anterior", "btn_siguiente", "Buscar");
    return function () {
        return datatable;
    };
})();




window.onload = getDataTable().init(0, 10);
