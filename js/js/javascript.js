

/*
 * @constructor
 * @param {type} tabla
 * @param {type} url
 * @param {type} txt_search
 * @returns {tabla}
 */
function tabla(tabla, url, txt_search) {



    var this_ = this;
    this.tabla = tabla;
    this.url = url;
    this.txt_search = txt_search;
    this.contador=0;
    this.init = function () {

        getDataTable().get_Json_data(this_.url);

    };

/*
 *
 * @param {type} tabla
 * @param {type} datos
 * @returns { create  table}
 *
 */
    this.Crear_tabla = function (tabla, datos) {

        var tbody = document.getElementById(tabla);
        var bucle;

        for (bucle in  datos) {
                this_.contador++;
            var row = document.createElement("TR");
            var td1 = document.createElement("TD");
            var td1 = document.createElement("button");
            var td2 = document.createElement("TD");
            var td3 = document.createElement("TD");
            var td4 = document.createElement("TD");
            var td5 = document.createElement("TD");
            var td6 = document.createElement("TD");
            td1.appendChild(document.createTextNode((this_.contador)));
            td2.appendChild(document.createTextNode((datos[bucle].nif)));
            td3.appendChild(document.createTextNode((datos[bucle].nom)));
            td4.appendChild(document.createTextNode((datos[bucle].ape1) + " "));
            td4.appendChild(document.createTextNode((datos[bucle].ape2)));
            td5.appendChild(document.createTextNode((datos[bucle].dataAlta)));
            td6.appendChild(document.createTextNode((datos[bucle].tipusRelacioClub)));



             row.appendChild(td1);
            row.appendChild(td2);
            row.appendChild(td3);
            row.appendChild(td4);
            row.appendChild(td5);
            row.appendChild(td6);
            tbody.appendChild(row);

            td1.setAttribute('id', this_.contador)
            td1.setAttribute('name',datos[bucle].nif)
           td1.style.backgroundColor = "transparent"
           td1.setAttribute('value',datos[bucle].nif);


           /*
            * no funciona
            *

             *td1.addEventListener('click',function(){
               alert("hola");
          var     variable = document.getElementsByClassName("button")[0].value;


          alert(variable);
           })

         */



   }


    };




  /*
   * get JSON
   *
   */

    this.get_Json_data = function (url) {
        var datos;
        var xmlhttp;

        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        ;
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                datos = JSON.parse(xmlhttp.responseText);


                getDataTable().Crear_tabla(this_.tabla, datos);



            }
            ;
        };
        xmlhttp.open("GET", url, false);
        xmlhttp.send(null);

    };
    /*
     * this function filter table
     *
     */
    this.filter = function (txt_search, tabla) {
        var phrase = document.getElementById(txt_search);
        var words = phrase.value.toLowerCase().split(" ");
        var table = document.getElementById(tabla);
        var ele;
        for (var r = 1; r < table.rows.length; r++) {
            ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
            var displayStyle = 'none';
            for (var i = 0; i < words.length; i++) {
                if (ele.toLowerCase().indexOf(words[i]) >= 0)
                    displayStyle = '';
                else {
                    displayStyle = 'none';
                    break;
                }
            }
            table.rows[r].style.display = displayStyle;
        }
    };



    document.getElementById("txt_search").onkeyup = function () {
        getDataTable().filter(this_.txt_search, this_.tabla);

    }





}
    /*
     *
     * @type Function|Function
     * create clousere and new object
     */
var getDataTable = (function () {
    var datatable = new tabla("tabla", "http://miguelet.ovh/pere/js/json.php", "txt_search");
    return function () {
        return datatable;
    };
})();


/*
 * initzialice
 *
 */

window.onload = getDataTable().init();



