

/*@contrsuctor
 *
 * @param {type} frm
 * @param {type} nif
 * @param {type} name
 * @param {type} last_name1
 * @param {type} last_name2
 * @param {type} tlf
 * @param {type} email
 * @param {type} birth_dates
 * @param {type} high_data
 * @param {type} relacio
 * @param {type} relacio_div
 * @param {type} text_area_span
 * @param {type} id_textarea
 * @param {type} street
 * @param {type} city
 * @param {type} province
 * @param {type} cp
 * @param {type} IBAN
 * @param {type} entity
 * @param {type} office
 * @param {type} account
 * @param {type} come_down
 * @param {type} data_baixa
 * @param {type} alert
 * @param {type} text_area_baja
 * @param {type} save
 * @returns {alta_js}
 *
 */
function alta_js(frm, nif, name, last_name1, last_name2, tlf, email, birth_dates, high_data, relacio,
        relacio_div, text_area_span, id_textarea, street, city
        , province, cp, IBAN, entity, office, account, come_down, data_baixa, alert, text_area_baja, save) {
    var this_ = this;
    this.frm = frm;
    this.nif = nif;
    this.name = name;
    this.last_name1 = last_name1;
    this.last_name2 = last_name2;
    this.tlf = tlf;
    this.email = email;
    this.birth_dates = birth_dates;
    this.birth_dates = birth_dates;
    this.high_data = high_data;
    this.relacio = relacio;
    this.relacio_div = relacio_div;
    this.text_area_span = text_area_span;
    this.id_textarea = id_textarea;
    this.street = street;
    this.city = city;
    this.province = province;
    this.cp = cp;
    this.IBAN = IBAN;
    this.entity = entity;
    this.office = office;
    this.account = account;
    this.come_down = come_down;
    this.alert = alert;
    this.office = office;
    this.account = account;
    this.text_area_baja = text_area_baja;
    this.save = save;
    /*
     * @validate text
     * @param {type} text
     * @returns {Boolean}
     *
     */
    this.validate_text_function = function (text) {
        var ok = this_.get_dades(text);
        return validate_text(ok);
    };


    /*
     * @validate dni
     * @param {type} text
     * @returns {Boolean}
     *
     */
    this.validate_dni = function (text) {
        var ok = this_.get_dades(text);
        return isDNI(ok);
    };



    /*
     * @validate cp
     * @param {type} text
     * @returns {Boolean}
     *
     */
    this.validate_code = function (text) {

        var ok = this_.get_dades(text);
        return validate_cp(ok);

    };

    /*
     * @validate IBAN
     * @param {type} text
     * @returns {Boolean}
     *
     */
    this.validate_IBAN_ = function (text) {

        var ok = this_.get_dades(text);
        return test_IBAN(ok);
    };


    /*
     * validate entity
     * @param {type} text
     * @returns {Boolean}
     *
     */
    this.validate_entity_ = function (text) {
        var ok = this_.get_dades(text);
        return validate_entity(ok);
    };
    this.validate_account_ = function (text) {
        var ok = this_.get_dades(text);
        return validate_account(ok);
    };

    /*
     * generic function for get value in input
     * @param {type} valor
     * @returns {Element.value}
     *
     */
    this.get_dades = function (valor) {
        return document.getElementById(valor).value;
    };
    /*
     * generic function for get div in input
     * @param {type} div
     * @returns {Element}
     *
     */
    this.get_id = function (div) {
        return  document.getElementById(div);
    };


    /*
     * validate banc
     * @returns {bolean}
     *
     */
    this.valida_banc = function () {
        var iban = this_.validate_IBAN_(this_.IBAN);
        var entity = this_.validate_entity_(this_.entity);
        var office = this_.validate_entity_(this_.office);
        var account = this_.validate_account_(this_.account);
        var valid = true;
        if (!account) {
            valid = false;
            this_.get_id(this_.account).style.borderColor = "red";
        } else {
            this_.get_id(this_.account).style.borderColor = "green";

        }
        ;
        if (!office) {
            valid = false;
            this_.get_id(this_.office).style.borderColor = "red";
        } else {
            this_.get_id(this_.office).style.borderColor = "green";
        }
        ;
        if (!entity) {
            valid = false;
            this_.get_id(this_.entity).style.borderColor = "red";
        } else {
            this_.get_id(this_.entity).style.borderColor = "green";
        }
        ;
        if (!iban) {
            valid = false;
            this_.get_id(this_.IBAN).style.borderColor = "red";
        } else {
            this_.get_id(this_.IBAN).style.borderColor = "green";
        }
        ;
        this_.ok1(valid);
    };


    /*
     * validate
     * @returns {boolean}
     *
     */
    this.valida = function () {
        var name = this_.validate_text_function(this_.name);
        var last_name1 = this_.validate_text_function(this_.last_name1);
        var last_name2 = this_.validate_text_function(this_.last_name2);
        var city = this_.validate_text_function(this_.city);
        var province = this_.validate_text_function(this_.province);
        var nif = this_.validate_dni(this_.nif);
        var cp = this_.validate_code(this_.cp);
        var valid = true;
        if (!name) {
            valid = false;
            this_.get_id(this_.name).style.borderColor = "red";

        } else {
            this_.get_id(this_.name).style.borderColor = "green";
        }
        ;
        if (!last_name1) {
            valid = false;
            this_.get_id(this_.last_name1).style.borderColor = "red";
        } else {
            this_.get_id(this_.last_name1).style.borderColor = "green";
        }
        ;
        if (!last_name2) {
            valid = false;
            this_.get_id(this_.last_name2).style.borderColor = "red";
        } else {
            this_.get_id(this_.last_name2).style.borderColor = "green";
        }
        ;
        if (!nif) {
            valid = false;
            this_.get_id(this_.nif).style.borderColor = "red";
        } else {
            this_.get_id(this_.nif).style.borderColor = "green";
        }
        ;
        if (!city) {
            valid = false;
            this_.get_id(this_.city).style.borderColor = "red";
        } else {
            this_.get_id(this_.city).style.borderColor = "green";
        }
        ;
        if (!province) {
            valid = false;
            this_.get_id(this_.province).style.borderColor = "red";
        } else {
            this_.get_id(this_.province).style.borderColor = "green";
        }
        ;
        if (!cp) {
            valid = false;
            this_.get_id(this_.cp).style.borderColor = "red";
        } else {
            this_.get_id(this_.cp).style.borderColor = "green";
        }
        ;
        if (this_.get_dades(this_.street) != "") {
            this_.get_id(this_.street).style.borderColor = "green";
        } else {
            this_.get_id(this_.street).style.borderColor = "red";
            valid = false;
        }
        ;
        if (this_.get_dades(this_.relacio).selectedIndex === 0) {
            valid = false;
            this_.get_id(this_.relacio).style.borderColor = "red";
        } else {
            this_.get_id(this_.relacio).style.borderColor = "green";
        }
        ;
        this_.ok(valid);
    };

    /*
     *
     * @param {type} valid
     * @returns { enable or disable buton}
     *
     */
    this.ok1 = function (valid) {
        if (valid === true) {
            this_.get_id(this_.save).disabled = false;
        } else {
            this_.get_id(this_.save).disabled = "disabled";
        }
        ;
    }
    /*
     *
     * @param {type} valid
     * @returns { enable or disable buton}
     *
     */
    this.ok = function (valid) {
        if (valid === true) {
            this_.get_id(this_.save).disabled = false;
        } else {
            this_.get_id(this_.save).disabled = "disabled";
        }
        ;
    };

    /*
     * functions onblour and onclick
     * @returns {undefined}
     *
     */


    this.get_id(this.name).onblur = function () {
        valida();

    };
    this.get_id(this.save).onclick = function () {
        enviar_ajax();

    };
    this.get_id(this.nif).onblur = function () {
        valida();
    };
    this.get_id(this.last_name1).onblur = function () {
        valida();
    };
    this.get_id(this.last_name2).onblur = function () {
        valida();
    };
    this.get_id(this.city).onblur = function () {
        valida();
    };
    this.get_id(this.province).onblur = function () {
        valida();
    };
    this.get_id(this.cp).onblur = function () {
        valida();
    };

    this.get_id(this.relacio).onblur = function () {
        valida();
    };
    this.get_id(this.IBAN).onblur = function () {
        valida_banc();
    };
    this.get_id(this.office).onblur = function () {
        valida_banc();
    };
    this.get_id(this.account).onblur = function () {
        valida_banc();
    };
    this.get_id(this.entity).onblur = function () {
        valida_banc();
    };



    /*
     *
     * @returns {send ajax}
     *
     *
     */
    this.enviar_ajax = function () {
        var nif = this_.get_dades(this_.nif);
        var name = this_.get_dades(this_.name);
        var last_name1 = this_.get_dades(this_.last_name1);
        var last_name2 = this_.get_dades(this_.last_name2);
        var tlf = this_.get_dades(this_.tlf);
        var email = this_.get_dades(this_.email);
        var birth_dates = this_.get_dades(this_.birth_dates);
        var high_data = this_.get_dades(this_.high_data);
        var relacio_id = this_.get_id(this_.relacio);
        var street = this_.get_dades(this_.street);
        var city = this_.get_dades(this_.city);
        var province = this_.get_dades(this_.province);
        var cp = this_.get_dades(this_.cp);
        var IBAN = this_.get_dades(this_.IBAN);
        var entity = this_.get_dades(this_.entity);
        var office = this_.get_dades(this_.office);
        var account = this_.get_dades(this_.account);
        var posicion = relacio_id.options.selectedIndex; //posicion
        var relacio = relacio_id.options[posicion].text;
        var json = {"nif": nif, "name": name, "last_name1": last_name1, "last_name2": last_name2, "tlf": tlf,
            "email": email, "birth_dates": birth_dates, "high_data": high_data, "relacio": relacio, "street": street,
            "city": city, "province": province, "cp": cp, "IBAN": IBAN, "entity": entity, "office": office,
            "account": account};

        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
       // change url!
        xmlhttp.open("POST", "URL");
        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        xmlhttp.send(JSON.stringify(json));
        var datos = JSON.stringify(json)
        console.log(datos);
    };


    document.getElementById("HOME").onclick = function () {
        window.location = "index.html"
    };

    document.getElementById("alta").onclick = function () {
        window.location = "alta.html";
    };



}
;

/*
 * validates!!
 * @param {type} value
 * @returns {Boolean}
 *
 */
function validate_account(value) {
    var test1 = /^([0-9]){10}$/;
    return test1.test(value);
}
function test_IBAN(name) {
    var test1 = /^([a-zA-Z]|[0-9]){4}$/;
    return test1.test(name);
}
function validate_cp(valor) {
    var text = /^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$/;
    return text.test(valor);
}
function validate_text(valor) {
    var text = /^\D\w{2,}$/;
    return text.test(valor);

}
function validate_entity(valor) {
    var text = /^([0-9]){4}$/;
    return text.test(valor);
}
function isDNI(dni) {
    var numero
    var let
    var letra
    var expresion_regular_dni
    var error = false;
    expresion_regular_dni = /^\d{8}[a-zA-Z]$/;
    if (expresion_regular_dni.test(dni) == true) {
        numero = dni.substr(0, dni.length - 1);
        let = dni.substr(dni.length - 1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero + 1);
        if (letra != let.toUpperCase()) {
            error = false;
        } else {
            error = true;
        }
    } else {
        error = false;
    }
    return error;


    /*
     *
     * @type return iban
     */

  function CalcularIBAN(numerocuenta, codigopais) {
            //Conversión de letras por números
            //A=10 B=11 C=12 D=13 E=14
            //F=15 G=16 H=17 I=18 J=19
            //K=20 L=21 M=22 N=23 O=24
            //P=25 Q=26 R=27 S=28 T=29
            //U=30 V=31 W=32 X=33 Y=34
            //Z=35

            if (codigopais.length != 2)
                return "";
            else {
                var Aux;
                var CaracteresSiguientes;
                var TmpInt;
                var CaracteresSiguientes;

                numerocuenta = numerocuenta + (codigopais.charCodeAt(0) - 55).toString() + (codigopais.charCodeAt(1) - 55).toString() + "00";

                //Hay que calcular el módulo 97 del valor contenido en número de cuenta
                //Como el número es muy grande vamos calculando módulos 97 de 9 en 9 dígitos
                //Lo que se hace es calcular el módulo 97 y al resto se le añaden 7 u 8 dígitos en función de que el resto sea de 1 ó 2 dígitos
                //Y así sucesivamente hasta tratar todos los dígitos

                TmpInt = parseInt(numerocuenta.substring(0, 9), 10) % 97;

                if (TmpInt < 10)
                    Aux = "0";
                else
                    Aux = "";

                Aux=Aux + TmpInt.toString();
                numerocuenta = numerocuenta.substring(9);

                while (numerocuenta!="") {
                    if (parseInt(Aux, 10) < 10)
                        CaracteresSiguientes = 8;
                    else
                        CaracteresSiguientes = 7;

                    if (numerocuenta.length<CaracteresSiguientes) {
                        Aux=Aux + numerocuenta;
                        numerocuenta="";
                    }
                    else {
                        Aux=Aux + numerocuenta.substring(0, CaracteresSiguientes);
                        numerocuenta=numerocuenta.substring(CaracteresSiguientes);
                    }

                    TmpInt = parseInt(Aux, 10) % 97;

                    if (TmpInt < 10)
                        Aux = "0";
                    else
                        Aux = "";

                    Aux=Aux + TmpInt.toString();
                }

                TmpInt = 98 - parseInt(Aux, 10);

                if (TmpInt<10)
                    return codigopais + "0" + TmpInt.toString();
                else
                    return codigopais + TmpInt.toString();

            }
        }






    /*
     *
     * @fins asi
     *
     *
     *
     */
}
/*
 * @ new object
 *
 *
 */
window.onload = alta_js("frm", "nif", "name", "last_name1", "last_name2", "tlf", "email", "birth_dates", "high_data", "relacio", "relacio_div", "text_area_span", "id_textarea", "street", "city"
        , "province", "cp", "IBAN", "entity", "office", "account", "come_down", "data_baixa", "alert", "text_area_baja", "save");
