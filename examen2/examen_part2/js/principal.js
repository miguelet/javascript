

/*
 * @constructor
 * @param {type} tabla
 * @param {type} url
 * @param {type} txt_search
 * @returns {tabla}
 */
function tabla(tabla, txt_search) {



    var this_ = this;
    this.tabla = tabla;
    this.url = "json/personas.json";
    this.txt_search = txt_search;
    this.contador = 0;
    this.datos = ""
    this.init = function (url) {
        this_.contador = 0;
        getDataTable().get_Json_data(url);

    };

    /*
     *
     * @param {type} tabla
     * @param {type} datos
     * @returns { create  table}
     *
     */
    this.Crear_tabla = function (tabla) {

        var tbody = document.getElementById(tabla);
        var bucle;

        for (bucle in  this_.datos) {
            this_.contador++;
            var row = document.createElement("TR");
            var td1 = document.createElement("TD");
            var td11 = document.createElement("a");
            var td2 = document.createElement("TD");
            var td3 = document.createElement("TD");
            var td4 = document.createElement("TD");
            var td5 = document.createElement("TD");
            var td6 = document.createElement("TD");
            var td7 = document.createElement("TD");
            var td77 = document.createElement("input");

            td77.type = 'checkbox';
            td77.name = 'check';


            td11.appendChild(document.createTextNode((this_.contador)));
            td2.appendChild(document.createTextNode((this_.datos[bucle].nif)));
            td3.appendChild(document.createTextNode((this_.datos[bucle].nom)));
            td4.appendChild(document.createTextNode((this_.datos[bucle].cognoms)));
            td5.appendChild(document.createTextNode((this_.datos[bucle].dataAlta)));
            td6.appendChild(document.createTextNode((this_.datos[bucle].tipusRelacioClub)));





            row.appendChild(td1);
            td1.appendChild(td11);
            row.appendChild(td2);
            row.appendChild(td3);
            row.appendChild(td4);
            row.appendChild(td5);
            row.appendChild(td6);
            row.appendChild(td7);
            td7.appendChild(td77);

            tbody.appendChild(row);


            td11.setAttribute('href', this_.datos[bucle].idPersona);
            td77.setAttribute('value', this_.datos[bucle].idPersona);
            td77.addEventListener('click', function () {
            });
        }
    };
    document.getElementById("select_all").onclick = function () {

        if (document.getElementById("select_all").checked) {

            var x = document.getElementsByName("check");
            var i;
            for (i = 0; i < this_.contador; i++) {
                x[i].checked = true;
            }

        } else {
            var x = document.getElementsByName("check");
            var i;
            for (i = 0; i < this_.contador; i++) {
                x[i].checked = false;
            }
        }

    };



    /*
     * get JSON
     *
     */

    this.get_Json_data = function (url) {
        var datos;
        var xmlhttp;

        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        ;
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                this_.datos = JSON.parse(xmlhttp.responseText);


                getDataTable().Crear_tabla(this_.tabla);
            };
        };
        xmlhttp.open("GET", url, false);
        xmlhttp.send(null);

    };
    /*
     * this function filter table
     *
     */
    this.filter = function (txt_search, tabla) {
        var phrase = document.getElementById(txt_search);
        var words = phrase.value.toLowerCase().split(" ");
        var table = document.getElementById(tabla);
        var ele;
        for (var r = 1; r < table.rows.length; r++) {
            ele = table.rows[r].innerHTML.replace(/<[^>]+>/g, "");
            var displayStyle = 'none';
            for (var i = 0; i < words.length; i++) {
                if (ele.toLowerCase().indexOf(words[i]) >= 0)
                    displayStyle = '';
                else {
                    displayStyle = 'none';
                    break;
                }
            }
            table.rows[r].style.display = displayStyle;
        }
    };
    document.getElementById("txt_search").onkeyup = function () {
        getDataTable().filter(this_.txt_search, this_.tabla);

    };
///
    document.getElementById("action").onchange = function () {
        if (document.getElementById("action").selectedIndex === 0) {
             document.getElementById("select_all").checked=false;
            var x = document.getElementsByName("check");
            var i;
            for (i = 0; i < this_.contador; i++) {
                x[i].checked = false;
            }
        } else if (document.getElementById("action").selectedIndex === 1) {
            if (document.getElementsByTagName("input").type === "checkbox".checked) {
                var a = [];
                var x = document.getElementsByName("check");
                var i;
                for (i = 0; i < this_.contador; i++) {
                    if (x[i].checked === true) {
                        a += x[i].value + ",";
                    }
                }
                var newStr = a.substring(0, a.length - 1);
                var jso = {"action": "DELET", "idsPersona": "[" + newStr + "]"};
                var final = JSON.stringify(jso);
                alert(final);
            }
        } else if (document.getElementById("action").selectedIndex === 2) {
            if (document.getElementsByTagName("input").type === "checkbox".checked) {
                var a = [];
                var x = document.getElementsByName("check");
                var i;
                for (i = 0; i < this_.contador; i++) {
                    if (x[i].checked === true) {
                        a += x[i].value + ",";
                    }
                }
                var newStr = a.substring(0, a.length - 1);
                var jso = {"action": "REMITTANCE", "idsPersona": "[" + newStr + "]"}
                var final = JSON.stringify(jso);
                alert(final);

            }


        }
    }
    this.deleteRow = function () {
        try {
            var table = document.getElementById(this_.tabla);
            var rowCount = table.rows.length;
            for (var i = 1; i < rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[0].childNodes[0];
                table.deleteRow(i);
                rowCount--;
                i--;
            }
        } catch (e) {
            alert(e);
        }

    };

    document.getElementById("HOME").onclick = function () {
        window.location = "index.html"
    };
    document.getElementById("listar").onclick = function () {
        getDataTable().deleteRow();
        var url = "json/personas.json";
        document.getElementById("oculto").style.display = 'block';
        getDataTable().init(url);
        document.getElementById("select_all").checked=false;

    };
    document.getElementById("soci").onclick = function () {
        getDataTable().deleteRow();
        var url = "json/soci.json";
        document.getElementById("oculto").style.display = 'block';
        getDataTable().init(url);
         document.getElementById("select_all").checked=false;
    };
    document.getElementById("transe").onclick = function () {
        getDataTable().deleteRow();
        var url = "json/transeunte.json";
        document.getElementById("oculto").style.display = 'block';
        getDataTable().init(url);
         document.getElementById("select_all").checked=false;
    };
    document.getElementById("alumne").onclick = function () {
        getDataTable().deleteRow();
        var url = "json/alumne.json";
        document.getElementById("oculto").style.display = 'block';
        getDataTable().init(url);
         document.getElementById("select_all").checked=false;
    };
    document.getElementById("alta").onclick = function () {
        window.location = "alta.html";
    };
};
/*
 *
 * @type Function|Function
 * create clousere and new object
 */
var getDataTable = (function () {
    var datatable = new tabla("tabla", "txt_search");
    return function () {
        return datatable;
    };
})();


/*
 * initzialice
 *
 */

window.onload = getDataTable().init("json/personas.json");

