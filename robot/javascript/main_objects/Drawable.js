/**
 * Creates the Drawable object which will be the base class for
 * all drawable objects in the simulator. Sets up defualt variables
 * that all child objects will inherit, as well as the defualt
 * functions. 
 * @author Pere Crespo Molina <pedcremo@iestacio.com> 
 * @class
 */
function Drawable() {   
    //private section
    var draggable=false;	
    
    this.init = function(x, y) {
        // Default variables
        this.x = x;
        this.y = y;
    }	;
    this.setDraggable= function(){
	draggable=true;
    };
    this.unsetDraggable= function(){
	draggable=false;
    };
    this.isDraggable=function(){
    	return draggable;
    };

    // Define abstract function to be implemented in child objects
    this.draw = function() {
    };

    this.convertToRadians = function(degree) {
            return degree*(Math.PI/180);
    };
    
}
