/**
* The robot object with all the properties and functions that are supposed to exist in a real linetracker Robot 
* @author Pere Crespo Molina <pedcremo@iestacio.com> 
* @class
*/
function Robot(img_src_) {
	//Private section
	var speedX = 5; // Redefine speed on X axis
	var speedY = 0; // Redefine speed on Y axis 
	
	var robot_img = new Image();	
	robot_img.src=img_src_;
	
	if (robot_img.width==0) robot_img.width=120;
	if (robot_img.height==0) robot_img.height=76;	
	
	var numsensors=8;
	var barsensor=new Array();// Defined by two 2D coordinates
	//this.drag = false;
	var starting_point=undefined; // X,Y point where sensors have detected a black line. 
	var match_point=undefined;// match_point is the nearest point of one of the drawed circuit to startingpoint
	var that=this;
	var offscreenCanvas = document.createElement('canvas');	
	var offscreenCtx = offscreenCanvas.getContext('2d');  	
	var start_race=false;

	//Public section
	this.width = robot_img.width;
	this.height =robot_img.height;
	this.angle = 360;
  	//this.x
	//this.y

  	/**
	* Restore initial robot properties to default values
	*/
  	

    	var rotateAndCache = function(image,angle) {
  		var size = Math.max(image.width, image.height);
  		offscreenCanvas.width = size;
  		offscreenCanvas.height = size;
		offscreenCtx.clearRect( 0 , 0 , image.width , image.height );
  		offscreenCtx.translate(size/2, size/2);
  		
		offscreenCtx.rotate(that.convertToRadians(angle));  	
		offscreenCtx.drawImage(image, -(image.width/2), -(image.height/2));
		
		var mx=that.x+image.width/2;
		var my=that.y+image.height/2;
		//p1=getPoint(mx, my, this.x, this.y, this.angle);
		p2=getPoint(mx, my, that.x+image.width, that.y, that.angle);
		p3=getPoint(mx, my, that.x+image.width, that.y+image.height, that.angle);
		//p4=getPoint(mx, my, this.x, this.y+image.height, this.angle);
		
		var pbs1X=Math.round(p3.x-((p3.x-p2.x)/3));
		var pbs1Y=Math.round(p3.y-((p3.y-p2.y)/3));
		var pbs2Y=Math.round(p3.y-((p3.y-p2.y)/3)*2);
		var pbs2X=Math.round(p3.x-((p3.x-p2.x)/3)*2);
					
		barsensor[0]={x:pbs1X,y:pbs1Y};

		var aux_x,aux_y;
		var cont=numsensors-2;
		for (i=1;i<numsensors-1;i++){
			aux_x=Math.round(pbs2X-((pbs2X-pbs1X)/(numsensors-1))*i);
			aux_y=Math.round(pbs2Y-((pbs2Y-pbs1Y)/(numsensors-1))*i);
			barsensor[cont]={x:aux_x,y:aux_y};
			cont--;
		}
		barsensor[numsensors-1]={x:pbs2X,y:pbs2Y};
				
		that.context.font = 'italic 12pt Calibri';
		if (readBarSensors()){
			that.context.fillText("We are on track. Robot angle in degrees "+Math.round(that.angle),10,25);
			if (match_point==undefined) simulator.menu.enablebutton("robot_race");
			
		}else{
			that.context.fillText("We are OUT of track. Robot angle in degrees "+Math.round(that.angle),10,25);
			if (match_point==undefined) simulator.menu.disablebutton("robot_race");	
		}
  		return offscreenCanvas;   
    }	
	
    var readBarSensors= function(){
		var c;
		var ontrack=false;	
		var reading_result=""; //String 00011000
		for (i=0;i<numsensors;i++){	
		try{
	 		c = that.context.getImageData(barsensor[i].x, barsensor[i].y, 1, 1).data;
			if (c[3]!=0){	
				ontrack=true;
				starting_point=barsensor[i];
				
				reading_result+="1";
				//break;
			}else{
		 		//ontrack=false;
		 		reading_result+="0";
			}
			}catch(err)	{
				ontrack=false;
			}
		}
		that.context.fillText("Bar sensor reading: "+reading_result,10,45);
		return ontrack;
    }
    
    /**
     * Start robot animation from their default or last position point
     */	
     	
    this.startRace = function(){
	start_race=true;
    	
    }

    /**
     * Stop robot animation from their default or last position point
     */	
     	
    this.stopRace = function(){
	start_race=false;
    }


    /**
    * Implement abstract function draw . Draw the robot in x,y location and with the proper angle
    */
    this.draw = function() {
        // Robot coordinates
	
	if (simulator.objects['drawcircuit'].getCircuitPathPoints()!=undefined && simulator.objects['drawcircuit'].finished){
	    var circuit_points=simulator.objects['drawcircuit'].getCircuitPathPoints();
		var dx,dy=0;
		
		if (match_point!=undefined){ 
			//We have the robot attached to one point of the circuit
			if (match_point==circuit_points.length){				
				dy=circuit_points[match_point-1].y-circuit_points[0].y;
				dx=circuit_points[match_point-1].x-circuit_points[0].x;
				match_point=0;
			}else if(match_point>2){
				dy=circuit_points[match_point].y-circuit_points[match_point-3].y;
				dx=circuit_points[match_point].x-circuit_points[match_point-3].x;	
			}
			
			this.angle=Math.atan2(dy,dx)*(180/Math.PI);//Arctang calculation
			
			var correct_width=-this.width/2;
			var correct_height=-this.height/2;
			
			this.x=circuit_points[match_point].x+(correct_width);
			this.y=circuit_points[match_point].y+(correct_height);
			
			match_point++;
		}else{
			if (starting_point!=undefined && start_race){
				match_point=0;
				var min_distance=100000;
				for (i=0;i<circuit_points.length;i++){
					var distance=Math.abs(starting_point.x-circuit_points[i].x)+Math.abs(starting_point.y-circuit_points[i].y);
					if (distance<min_distance){
						match_point=i;
						min_distance=distance;
					}			
				}
			}
        	}
		try{	
			//function NotNumberException() {};
			//var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
			var plusOrMinus=0;
			if (isNaN(this.x) || isNaN(this.y)) throw new Error("Empty initial x,y coordinates");
			this.context.drawImage(rotateAndCache(robot_img,this.angle+(Math.random() * 3)*plusOrMinus), this.x, this.y-19);
		}catch(err){
			console.log(err+" we can draw a robot without knowing their initial position");
		}
	}
	//this.context.drawImage(imageRepository.robot, this.x, this.y);
    }
    //End Draw method
    
}
//End Robot object definition

// Set Robot to inherit properties from Drawable abstract class
Robot.prototype = new Drawable();
Robot.prototype.reset = function(){
  	  this.angle=360;
  	  this.x=100;
  	  this.y=100;
  	  starting_point=undefined;  	  
  	  match_point=undefined;  	  
  	}