/**
 * Creates the Background object which will become a child of
 * the Drawable object. The background is drawn on the "background"
 * canvas and creates the ilusion of moving by panning the image.
 * @author Pere Crespo Molina <pedcremo@iestacio.com> 
 * @class
 */
function Circuit() {
    var that = this;
    /** 
     *  Implement abstract function drawing a circuit previously drawed by the DrawCircuit module
     */
 this.draw = function () {
        if (that.context)
            that.context.beginPath();
        else
            alert("Lost in the labyrinth");
        //that.context.strokeStyle = '#000000';
        simulator.objects['drawcircuit'].redrawSmoothed();
    };
    
    
/*
    
    this.c

   */
}
;





/*
 * var img = new Image();
 img.src = "imagenes/logo_aulambra.png";
 Lo siguiente será insertar la imagen en el CANVAS. Para ello en contexto del CANVAS nos ofrece el método .drawImage() El método .drawImage() recibe como parámetros la referencia a la imagen que acabamos de crear y las coordenadas x,y del Canvas a partir de las cuales posicionaremos la imagen.
 
 ctx.drawImage(img, 0, 0);
 */




// Set Circuit to inherit properties from Drawable
Circuit.prototype = new Drawable();
Circuit.prototype.save = function () {

        var canvas = document.getElementById("background");
        var dataUrl = canvas.toDataURL(); // obtenemos la imagen como png

        dataUrl = dataUrl.replace("image/png", 'image/svg'); // sustituimos el tipo por octet
        document.location.href = dataUrl;

    };  
    
    
    Circuit.prototype.load = function () {

        document.getElementById('file-input').onchange = function (e) {
 
            loadImage(
                    e.target.files[0],
                   
            function (img) {
                     var canvas = document.getElementById("background");
               document.body.appendChild(img);
                    console.log(img);
                    
                   //  canvas.background=(img);
                  //   canvas.drawImage(img, 0, 0);
                    },
                    
                    { 
                        maxWidth: 600,
                      canvas: true
                   } // Options
                            
            );
        };

    };
