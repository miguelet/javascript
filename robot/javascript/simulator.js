/**
 * Creates the simulator object which will hold all objects and data simulated.
 *  
 * @author Pere Crespo Molina <pedcremo@iestacio.com> 
 * @class
*/
function Simulator() {
	
	//Private section
	var that=this;

	/**
	* Viewport width
	* @private
	*/
 	var vpWidth=window.innerWidth
		|| document.documentElement.clientWidth
		|| document.body.clientWidth;

	/**
	* Viewport Height
	* @private
	*/
	var vpHeight=window.innerHeight
		|| document.documentElement.clientHeight
		|| document.body.clientHeight;
	
	var animation_interval=undefined; //Object devoted to store setinterval timing function

	//Public section
	this.objects=[]; //Objects that compose the simulation scene (Robot, Circuit, DrawCircuit...)	
	this.bgCanvas = document.getElementById('background');// Get the html5 canvas element	
	this.dragsystem=new DraggableController();//Singleton class in charge of managing canvas objects dragging system
	this.menu = new Menu(this.bgCanvas);//Create the menu

	/**
	* Gets canvas information and context and sets up all simulator
	* objects. 
	* Returns true if the canvas is supported and false if it
	* is not. This is to stop the animation script from constantly
	* running on older browsers.
	* @constructor
	*/
	this.init = function() {
		
		//viewporte();
		this.bgCanvas.width=vpWidth-50;
		this.bgCanvas.height=vpHeight-50;	
	
		// Test to check if canvas is supported
		if (this.bgCanvas.getContext) {
        		var bgContext = this.bgCanvas.getContext('2d');
			// Initialize objects to contain their context and canvas
			// information
			Circuit.prototype.context = bgContext;
			Circuit.prototype.canvasWidth = this.bgCanvas.width;
			Circuit.prototype.canvasHeight = this.bgCanvas.height;
   
			Robot.prototype.context = bgContext;
			Robot.prototype.canvasWidth = this.bgCanvas.width;
			Robot.prototype.canvasHeight = this.bgCanvas.height;
		
			// Initialize the objects that are drawed inside the canvas
			this.objects['drawcircuit']=new DrawCircuit(this.bgCanvas);  
			this.objects['circuit'] = new Circuit();	
			this.objects['robot'] = new Robot("imatges/robot_velocista_roig_xicotet.png");
			this.objects['robot'].setDraggable(); //Make it draggable
			this.objects['robot'].init(80,60-(this.objects['robot'].height/2)); //set draw points X & Y

			//this.objects['robot2'] = new Robot("imatges/robot_velocista_roig_xicotet.png");
			//this.objects['robot2'].setDraggable(); //Make it draggable
			//this.objects['robot2'].init(80,60-(this.objects['robot2'].height/2)); //set draw points X & Y

				    
			this.menu.addbutton("draw_circuit","Draw circuit",this.objects['drawcircuit'].enableDrawingEvents);
			this.menu.addbutton("draw_finished","Circuit OK",this.objects['drawcircuit'].disableDrawingEvents);	
			this.menu.addbutton("robot_race","Start race",this.start);		
			this.menu.addbutton("robot_race_stop","Stop race",this.stop);		
		
			this.menu.addbutton("race_step","Step by step",this.step);
			this.menu.addbutton("Show_path_points","Show path points",this.show_points);
                        
                        
                        
                        this.menu.addfile("file-input","load_circuit",this.objects['circuit'].load);
			this.menu.addbutton("save circuit","save circuit",this.objects['circuit'].save);
		
                                
                
                
                
                
			this.menu.disablebutton("draw_finished");
			this.menu.disablebutton("robot_race");		
			this.menu.disablebutton("robot_race_stop");	
			this.menu.disablebutton("race_step");
		
			this.menu.setTextBanner("First of all draw the circuit where we will race the car. Please press 'Draw circuit' button to start the drawing");		
			return true;
      	} else {
			return false;
      	}
    	}; //End init function
    
    
  
    this.show_points =function(){
	//var num_points=that.objects['drawcircuit'].getCircuitPathPoints().length;
	
	alert(that.objects['drawcircuit'].getCircuitPathPoints());
    }
    /** Start the animation loop */
    this.start = function() {
	that.menu.disablebutton("robot_race");    	
    	that.menu.enablebutton("robot_race_stop");
	for (var key in that.objects){
			var bot_=that.objects[key];
			if (bot_ instanceof Robot){
				bot_.startRace();
			}
	}
        //game.background.draw(); 
        that.animate(); 
        animation_interval=setInterval(that.animate, 100 / 35); //300 frames per second 1000/35
        
    };
    /** Stop  animation loop */
    this.stop = function() {
	
	that.menu.enablebutton("robot_race");	
	that.menu.disablebutton("robot_race_stop");	
	that.menu.enablebutton("race_step");
	for (var key in that.objects){
			var bot_=that.objects[key];
			if (bot_ instanceof Robot){
				bot_.stopRace();
			}
	}
        clearInterval(animation_interval);
        
    };
    /** Animate step by step */
    this.step = function() {
	//alert("Stop");
		that.menu.enablebutton("robot_race");	
		that.menu.disablebutton("robot_race_stop");	
        	clearInterval(animation_interval);
        	that.animate();
    };
    /**
    * The animation loop.We iterate over all drawable objects stored in the simulator instance and
    * redraw it
    */
    this.animate = function() {    
    	for (item in simulator.objects){
    		if (simulator.objects[item] instanceof Drawable)
    			simulator.objects[item].draw();
    }     
}
}//End Simulator Object


