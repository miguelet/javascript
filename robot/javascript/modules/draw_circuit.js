/** @module simulator/modules */

/** 
* This object allows user draw interactively any random circuit using the mouse as a drawing tool
*
* @author Pere Crespo Molina <pedcremo@iestacio.com> 
* @class
*/

function DrawCircuit(canvas_){

	//Private section
	var that=this; //Workaround
	var lastClick = [0, 0];
	var bgCanvas=canvas_;
	var bgContext = canvas_.getContext('2d');	
	var my_path_points = new Array();
	var point_counter = 0;
	var paint=false;

	//Public section
    	this.finished=false; //When circuit is completed true
	
	/** 
	* Return an array of points describing the circuit path
	* @returns {Number|Array} 
	*/
	this.getCircuitPathPoints=function(){
		return my_path_points;
	}
	
	/** 
	*   Enable events allowing drawing a circuit
	*/
	this.enableDrawingEvents = function(){
		my_path_points = undefined;	
		lastClick = [0, 0];
		clearCanvas();

		
		for (var key in simulator.objects){
			var bot_=simulator.objects[key];
			if (bot_ instanceof Robot){
				bot_.reset();
			}
		}
	
		try{
		   bgCanvas.removeEventListener('click',simulator.dragsystem.are_you_draggable,false);//Everytime we make a dblclick in the canvas we check if the object clicked it's draggable
		}catch(err){
		}

		bgCanvas.addEventListener('mousedown',setDraw,false);
		bgCanvas.addEventListener('mousemove',drawPoints,false);
		bgCanvas.addEventListener('mouseup',unsetDraw,false);	
		
		simulator.menu.disablebutton("draw_circuit");
		simulator.menu.enablebutton("draw_finished");
		simulator.menu.disablebutton("robot_race");		
		simulator.menu.disablebutton("race_step");
		
		simulator.menu.setTextBanner("Draw the circuit maintaining left mouse button pressed while you trace the path");
	}
	/** 
	*   Disable events forbidding drawing a circuit
	*/

	this.disableDrawingEvents = function(){
		bgCanvas.removeEventListener('mousedown',setDraw,false);
		bgCanvas.removeEventListener('mousemove',drawPoints,false);
		bgCanvas.removeEventListener('mouseup',unsetDraw,false);
		bgCanvas.addEventListener('click',simulator.dragsystem.are_you_draggable,false);//Everytime we make a dblclick in the canvas we check if the object clicked it's draggable
		
		simulator.menu.disablebutton("draw_finished");
		simulator.menu.enablebutton("draw_circuit");
		//if (that.my_path_points!=undefined){
		for (var key in simulator.objects){
			var bot_=simulator.objects[key];
			if (bot_ instanceof Robot){
				bot_.draw();
			}
		}
		//}
		
		simulator.menu.setTextBanner("Now drag the robot and drop it inside the circuit point you prefer as starting point"); 
	}
	
	// Clears the canvas.
	function clearCanvas() {   
	   bgContext.clearRect(0, 0,bgCanvas.width,bgCanvas.height);
	   
	}
	/*function merda(e){
		alert("merda");
	}*/
	function setDraw(e){
	  clearCanvas();
	  my_path_points=[]; //Empty the path points
	  x = getCursorPosition(e)[0] - this.offsetLeft;
	  y = getCursorPosition(e)[1] - this.offsetTop;	
	  
	  //Init point of my circuit
	  point_counter=0;
	  my_path_points[point_counter]={x:x,y:y};
	  point_counter++;
	  
	  lastClick = [x, y];
	  paint=true;
	  
	}

	function unsetDraw(e){
	  paint=false;
	  
	  //Smooth curves redrawing
	  that.redrawSmoothed();
	  that.finished=true;
	  simulator.menu.setTextBanner("If you are satisfied with the result, press 'Circuit Ok' otherwise, press mouse left button again and redraw a new circuit again");
	 
	}
	/**
	* It draws the circuit following the array path points described in the user drawing but trying to smooth the trace
	*/
	this.redrawSmoothed= function(){
	   clearCanvas();	
	   if (my_path_points!=undefined){
		   var num_points=my_path_points.length;
		   if (num_points-2>=0){
			  bgContext.moveTo(my_path_points[0].x,my_path_points[0].y)
			  var inc=5; 
			  for (i = 1; i < num_points - 2; i=i+inc)
			   {
				  
				  if ((i+inc)>=num_points){inc=1;}
				  var xc = (my_path_points[i].x + my_path_points[i + inc].x) / 2;
				  var yc = (my_path_points[i].y + my_path_points[i + inc].y) / 2;
				  bgContext.quadraticCurveTo(my_path_points[i].x, my_path_points[i].y, xc, yc);
			   }
			  // curve through the last two points
			  
			 bgContext.quadraticCurveTo(my_path_points[num_points-2].x, my_path_points[num_points-2].y, my_path_points[num_points-1].x,my_path_points[num_points-1].y);
			 bgContext.moveTo(my_path_points[num_points-1].x,my_path_points[num_points-1].y);
			 bgContext.lineTo(my_path_points[0].x,my_path_points[0].y);
			 bgContext.lineWidth = 3;
			 bgContext.strokeStyle = '#000000'; 
			 bgContext.stroke();				
			}
		}
	}

	function drawPoints(e) {
	   if (paint){
	   	   that.finished=false;
		   x = getCursorPosition(e)[0] - this.offsetLeft;
		   y = getCursorPosition(e)[1] - this.offsetTop;	
		   bgContext.beginPath();   
		   bgContext.moveTo(lastClick[0], lastClick[1]);
		   bgContext.lineTo(x, y, 6);
		   bgContext.lineWidth = 5;
		   bgContext.strokeStyle = '#000000';
		   bgContext.stroke();
		   lastClick = [x, y];
		   my_path_points[point_counter]={x:x,y:y};
		   point_counter++;		
	    }
	}	
}
