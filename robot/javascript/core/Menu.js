/**
* Object Simulator Top Menu. It's a generic component and we can enable or disable the options and attach function objects to trigger execution
* @author Pere Crespo Molina <pedcremo@iestacio.com> 
* @class
*/
function Menu(canvas_){
	var buttons={};//Associative array
	var canvas=canvas_; //We put our menu before this canvas object
	
	var menu_layer=document.createElement("div");
	menu_layer.setAttribute("style","height=50px; width=100%; display:inline;");
	var menu_banner=document.createElement("div");
    	menu_banner.setAttribute("style","margin:right; width:100%; font-size:14px; font-color:#ff0000;");
    	menu_layer.appendChild(menu_banner);
    	document.body.insertBefore(menu_layer,canvas);
    	/**
	*   Add a new button to the top menu.
	*   @param {string} id Html DOM element identifier for the button
	*   @param {string} text Text tittle added to the button
	*   @param {function} A funtion object triggered when the button is pressed
	*/
	this.addbutton=function(id, text,func_object){
		var _btn=document.createElement("button");
		
		_btn.id=id;
		_btn.appendChild(document.createTextNode(text));   
		_btn.addEventListener('click',func_object,false);		
	 	//document.body.insertBefore(_btn,canvas);
		menu_layer.appendChild(_btn);
		//eval("buttons."+id+"=_btn");
		buttons[id]=_btn;
	}
        
        
        
        this.addfile=function(id, text,func_object){
		var _btn=document.createElement("input");
		//_btn.setAttribute("type", "type");
                 _btn.type = "file";
		_btn.id=id;
		_btn.appendChild(document.createTextNode(text));   
		_btn.addEventListener('click',func_object,false);		
	 	//document.body.insertBefore(_btn,canvas);
		menu_layer.appendChild(_btn);
		//eval("buttons."+id+"=_btn");
		buttons[id]=_btn;
	}
        
        
	/**
	* Especial html area where we put messages in order to help user to use the application
	* @param {string} Text to be included in the area
	*/
	this.setTextBanner=function(text){	
		menu_banner.innerHTML=text;
	}
	/**
	*   Disable the button so it can't be used
	*   @param {string} id Html DOM element identifier for the button
	*/
	this.disablebutton=function(id){
		var _btn=buttons[id];
		_btn.disabled=true;
	}
	/**
	*   Enable the button so it can be used
	*   @param {string} id Html DOM element identifier for the button
	*/
	this.enablebutton=function(id){
		var _btn=buttons[id];
		_btn.disabled=false;
	}
}
