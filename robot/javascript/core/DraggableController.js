/**
 * Creates the DraggableController object which will be the base controller class for
 * all the drawable objects defined by the user as draggable in the simulator. 
 * As we have a limitation in wich is impossible to assign independent events to every
 * object drawed on the canvas we must implement a kind of driver like this if we want to simulate the process.
 * Another alternative could be use specialized frameworks as kinectJS but its use is forbidden in this course because we must enforce our pupils using javascript alone because didactic purposes. 
 * @author Pere Crespo Molina <pedcremo@iestacio.com> 
 * @class
 */
function DraggableController() {   
        //Private section
	var drag = false;	
   	var that=this;	
		
	this.are_you_draggable = function(e){			
		x = getCursorPosition(e)[0] - this.offsetLeft;
	  	y = getCursorPosition(e)[1] - this.offsetTop;

		for (var key in simulator.objects) {
		   var object_=simulator.objects[key];	
		   if (object_ instanceof Drawable && object_.isDraggable()){	
			
		   	if (x<=object_.x+object_.width && x>=object_.x && y <=object_.y+object_.height && y>=object_.y){
				that.object=object_;
				//alert("Mouse pointer x="+x+" y="+y+" Robot x="+that.x+" y="+that.y);
				simulator.bgCanvas.addEventListener('mousedown',grabObject,false);
				simulator.bgCanvas.addEventListener('mousemove',moveObject,false);
				simulator.bgCanvas.addEventListener('mouseup',dropObject,false);
		   	}  			
		   }			
		 }
			
	}
	function grabObject(e){
		drag=true;		
	}
	function moveObject(e){
		if(drag){
			
			x = getCursorPosition(e)[0] - this.offsetLeft;
			y = getCursorPosition(e)[1] - this.offsetTop;	
			that.object.x=x;
			that.object.y=y;						
			simulator.animate();
		}
	}
	function dropObject(e){
		drag=false;
		simulator.bgCanvas.removeEventListener('mousedown',grabObject,false);
		simulator.bgCanvas.removeEventListener('mousemove',moveObject,false);
		simulator.bgCanvas.removeEventListener('mouseup',dropObject,false);
	}    
}

