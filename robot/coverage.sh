#!/bin/bash
echo "Obri el navegador a http://localhost:8080/jscoverage.html?index.html per comprovar el grau de cobertura de la teua aplicació manualment"

echo "Obri el navegador a http://localhost:8080/jscoverage.html?/tests/index.html per comprovar el grau de cobertura dels teus tests"

java -Dfile.encoding=UTF-8 -jar JSCover-0.2.2/target/dist/JSCover-all.jar -ws --branch --document-root=. --report-dir=target

