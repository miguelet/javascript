﻿
module("Robot Making",{
	setup: function() {	
		this.robot = new Robot();
		imageRepository = new function() {
    			 this.robot = new Image();    		        
    			 this.robot.src = "../imatges/robot_velocista_groc_xicotet.png" ;   	
		}
		
		this.robot.context=document.createElement('canvas').getContext('2d'); 
		
	}, teardown: function() {
		delete this.robot;		
	}
});


test( "Regular robot creation", function() {
		//1
		equal( this.robot.offscreenCanvas, undefined, "We expect not to access a private property offscreenCanvas" );
		//2
		equal( this.robot.nom, undefined, "We expect not to access a private property named 'nom' because doesn't exist" );
		//3
		ok( this.robot.context instanceof Object, "We expect to access a public  property named 'context' because We need it to draw" );
		//4
		equal(this.robot.x, undefined,"X parameter when init has not been called yet is a NaN");
		/*raises(function(){
			this.robot.draw();
		},"You can't draw if you don't init the robot in a x,y coordinate");
		*/
		this.robot.init(0,0);
		//5	
		notEqual(this.robot.x,undefined, "x parameter is now a number");
		//6
		notEqual(this.robot.y,undefined, "y parameter is now a number");
		this.robot.draw();		
});

/*test ("Another battery test", function(){

	
});
*/

