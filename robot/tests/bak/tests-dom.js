﻿module("Creació de cartes a l'arbre DOM",{
	setup: function() {
		
		this.jugador1 = new Jugador("Pere");
		this.banca = new Jugador("banca");
		this.banca.setTipus("BANCA");
		this.partida= new Partida(this.jugador1,this.banca);
		
		this.carta1_bastos=new Carta('1', 'bastos',1);
		this.carta12_espases=new Carta('12', 'espadas',0.5);
		this.carta4_bastos=new Carta('4', 'bastos',4);
		
	}, teardown: function() {
		delete this.jugador1;
		delete this.carta1_bastos;
		delete this.carta12_espases;
		delete this.carta4_bastos;
	}

});

test( "Pintar cartes jugador normal", function() {
		
		
		var $fixture = $( "#qunit-fixture" );
		console.log(this.partida.getSetimigEngine().pintarCarta(this.carta1_bastos, 0, 0,"NORMAL"));
		$fixture.append(this.partida.getSetimigEngine().pintarCarta(this.carta1_bastos, 0, 0,"NORMAL"));
		equal(1,$(".carta_meua",$fixture).length,"Hem pintat 1 de bastos i tenim una capa carta_meua");
		$fixture.append(this.partida.getSetimigEngine().pintarCarta(this.carta12_espases, 0, 1,"NORMAL"));
		equal(2,$(".carta_meua",$fixture).length,"Hem pintat 1 de bastos i 12 espases per tant tenim 2 capes carta_meua");
		equal(1,$('#NORMAL_0_0',$fixture).length,"Id #NORMAL_0_0 per a la capa amb 4 de bastos");	
		
		equal("images/baralla/bastos/bastos_1.jpg",$('#NORMAL_0_0',$fixture).children(":first").attr('src'),"El src del tag img per a la carta 1 de bastos es images/baralla/bastos/bastos_1.jpg");	
		//Ocultem carta
		this.carta1_bastos.setOculta(true);
		$fixture.append(this.partida.getSetimigEngine().pintarCarta(this.carta1_bastos, 0, 2,"NORMAL"));
		equal("images/baralla/revers_small.jpg",$('#NORMAL_0_2',$fixture).children(":first").attr('src'),"El src del tag img per a la carta 1 de bastos es images/baralla/bastos/bastos_1.jpg");	
		
});

test("Pintar cartes jugador Banca", function() {


	var $fixture = $( "#qunit-fixture" );
	$fixture.append(this.partida.getSetimigEngine().pintarCarta(this.carta1_bastos, 0, 0,"BANCA"));
	equal(1,$(".carta_banca",$fixture).length,"Hem pintat 1 de bastos i tenim una capa amb classe carta_banca");
		
	
});


test("Pintem carta oculta primera carta Banca", function(){
	var $fixture = $( "#qunit-fixture" );
	this.carta1_bastos.setOculta(true);
	$fixture.append(this.partida.getSetimigEngine().pintarCarta(this.carta1_bastos, 0, 0,"BANCA"));
	equal(1,$('#BANCA_0_0',$fixture).length,"Id #BANCA_0_0 per a la capa amb 1 de bastos");	
	equal("images/baralla/revers_small.jpg",$('#BANCA_0_0',$fixture).children(":first").attr('src'),"El src del tag img per a la carta 1 de bastos es images/baralla/bastos/bastos_1.jpg");	
});

test("Pintar puntuació d'un jugador i banca",function(){
	var $fixture = $( "#qunit-fixture" );	
	equal(this.partida.getSetimigEngine().pintarPuntuacio(this.jugador1).html().length,0, "No tenim cap puntuació definida");
	this.jugador1.afegir_carta_a_jugada_actual(this.carta1_bastos);
	$fixture.append(this.partida.getSetimigEngine().pintarPuntuacio(this.jugador1));
	
	ok(0 < $('#punts_jugador1',$fixture).html().length, "Tenim algo de puntuacio després d'afegir una carta a la jugada actual");
	$fixture.append(this.partida.getSetimigEngine().pintarPuntuacio(this.jugador1));
	ok(0 < $('#punts_jugador1',$fixture).html().length, "Tornem a pintar puntuació per segona vegada");
	
	$fixture.append(this.partida.getSetimigEngine().pintarPuntuacio(this.banca));
	ok(0 == $('#punts_banca',$fixture).html().length, "No tenim puntuació encara a la banca");
	this.banca.afegir_carta_a_jugada_actual(this.carta1_bastos);
	equal(0 ,$('#punts_banca',$fixture).html().length, "No tenim res de puntuacio després a la banca pq jugador1 està jugant");
	this.jugador1.getJugadaActual().tancarJugada();
	ok(!this.jugador1.estaJugant(),"Jugador1 ha tancat jugada i no està jugant");
	$fixture.append(this.partida.getSetimigEngine().pintarPuntuacio(this.banca));
	ok(0 <$('#punts_banca',$fixture).html().length, "No tenim res de puntuacio després a la banca pq jugador1 està jugant");
});

test( "Invalidar jugada", function() {
	var $fixture = $( "#qunit-fixture" );	
	$fixture.append(this.partida.getSetimigEngine().invalidarJugada(0,"NORMAL"));
	console.log(this.partida.getSetimigEngine().invalidarJugada(0,"NORMAL").html());
	equal("images/x.png" ,$('img',$fixture).attr("src"), "Invalidem jugada");


});
