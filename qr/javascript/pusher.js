// incluim els 2 scripts que ens fan falta 
  document.write("<script src = 'javascript/valida.js'></script>")
  document.write("<script src = 'javascript/pintar_html.js'></script>")
  //acomensem en el codic
function init(txt_campo_isb,btn_buscar,div_error,div_ejemplo,div_conectado){
   //definim variables
    var ws = undefined;
    var campo_ISBN =  document.getElementById(txt_campo_isb);
     var isbn = campo_ISBN.value;
    // mirem si el navegador te sopòrt per a ws
    if (window.WebSocket){
        // si en te crearem la conexio
     ws = new WebSocket("ws://miguelet.ovh:9999");
        // la obrim
        ws.onopen = function (){
        console.log("conected");
    };
    // cuan recibim un dades del server
    ws.onmessage = function (evt){
     console.log(evt.data);
        var received_msg = evt.data;
        campo_ISBN.value = received_msg;
        libro(campo_ISBN.value);
    };
    // per a tancarla
    ws.onclose = function () {};                
    }else{
        // si no te soport d'euriem de fer-ho per ajax
        console.log ("no web soket");
    }
    // anyadim a el text  el event de onkeyup  i li asignem una funcio
 campo_ISBN.onkeyup=function(){enlazar();};      
    //validem i fer varies operacions en el isbn
 function enlazar() {
   
    if (is_valid_isbn( document.getElementById(txt_campo_isb).value)) {       
        var txt = campo_ISBN.value.replace(/-/g, "");
        if (txt.length == 10 || txt.length == 13) {
            if (valido_existente_isbn(txt)) {
               // si es valid habilitem el boto i anem a agafar el json de la web de pere
               valid_ISB(btn_buscar,div_error); 
                var isbn = campo_ISBN.value;
                btn_buscar.onclick =  libro(isbn);            
            } else {
                no_valid(div_error);
            }
        } else {
           no_valid(div_error);
        }
    } else {      
    }
} 
// funcio per agafar el json de la web de pere per ajax
   function libro(url) {
       var obj;
    var xmlhttp;
    if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
    } else {// code for IE6, IE5
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            obj = JSON.parse(xmlhttp.responseText);
           
            plena(obj,div_ejemplo,div_conectado);
        }
    };
    xmlhttp.open("GET", "http://pere.bocairent.net/z39.php?isbn=" + url, false);
    xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=ISO-8859-1');
    xmlhttp.send(null);
} 
}
//  carregem el js en el html
window.onload = new init("campo_isb","buscar","error","ejemplo","conectado");