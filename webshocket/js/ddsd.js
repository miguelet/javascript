/**
 * Created by miguelet on 05/02/15.
 */
$(document).ready(function(){
    $('#pages').perculator();
});


(function ($) {
    $.fn.perculator = function(options) {
        //This is the easiest way to have default options.
        var settings = $.extend({
            //These are the defaults
            vertical_position: 65,                      //From bottom to the top div of the plugin
            page_height:520,
            page_width: 800,                            //If width it's lesser than viewport with this parameter should be discarted
            plugin_style: 'default',
            slide_offset:50
        }, options);

        var perculator = $(this);

        var contenido = $(this).html();

        var imagen_page_fondo;

        $(this).html('');
        $(this).prepend('<div class="wrapper" style="{width:10000px}">' + contenido +'</div>');

        $(this).css({
            'position': 'absolute',
            'width': 100+'%',
            'height': settings.page_height + 'px',
            'bottom': settings.vertical_position + 'px',
            'overflow': 'auto',
            'overflow-x': 'hidden',
            'overflow-y': 'hidden'
        });

        $(this).find('.text_page').css({
            'position' : 'absolute',
            'width': settings.page_width + 'px',
            'max-width': '800px',                           //calculate from the viewport
            'height': '520px'
        });

        $(this).find('.title').css({                        //style of description class
            'position' : 'absolute',
            'width' : settings.page_width + 'px',
            'height' : '160px',
            'top' : '0px',
            'box-sizing' : 'border-box',
            'padding' : '0px 15px 0px 15px',
            'opacity' : '0',
            'background-color' : 'transparent',
            'text-shadow': '2px 2px 1px rgba(33, 33, 33, 1)',
            'font-size' : '52px',
            'font-weight' : 'bold',
            'color': 'white'
        });

        $(this).find('.resalt').css({
            'font-size' : '68px'
        });

        $(this).find('.block_text').css({                   //style of block_test class
            'position' : 'absolute',
            'width': settings.page_width + 'px',
            'height': (settings.page_height - 160) + 'px',    //160px reserved to title
            'top' : '160px',
            'background-size' : settings.page_width / 2 + 'px ' +  (settings.page_height - 160) + 'px'
        });

        $(this).find('.description').css({                  //style of description class
            'width' : settings.page_width / 2 + 'px',
            'height' : (settings.page_height - 160) + 'px',  //160px reserved to title
            'box-sizing' : 'border-box',
            'padding': '40px',
            'background-color' : 'white',
            'display':'inline-block'
        });

        $(this).find('img.item_page_background').css({      //style of item_page_background class
            'visibility' : 'hidden',
            'display' : 'none'
        });

        $(this).find('.capital').css({                      //style of capital class
            'float' : 'left',
            'background' : 'white',
            'font-size' : '50px',
            'line-height' : '30px',
            'padding' : '2px',
            'margin-right' : '5px'
        });

        $(this).find('.footer_text').css({                   //style of footer_text class
            'position' : 'absolute',
            'left' : '75%',
            'bottom' : '10px',
            'padding' : '5px 10px 5px 10px',
            'background-color' : 'white',
            'font-size' : '20px',
            'color' : '#454545'
        });

        $("#pages .footer_text").each(function(index,value){
            $(this).css("margin-left",-$(this).outerWidth()/2);
        });

        var init_pos = $(document).outerWidth() * 3 / 4;    // this measuring  the viewport
        var prev_layer_pos = 0;

        $(this).find('.text_page').each(function(index){

            $(this).find('img.item_page_background').css({'visibility': 'hidden', 'display': 'none'});
            imagen_page_fondo = $(this).find('img.item_page_background').attr('src');

            titulo = $(this).find('div.title').html();

            var that = $(this);                                 //text_page class

            if (index == 0){
                that.css("left", init_pos + "px");
                prev_layer_pos += init_pos + settings.page_width + settings.slide_offset;
            } else {
                that.css({"left" : prev_layer_pos + "px"});
                prev_layer_pos += settings.page_width + settings.slide_offset;
            }

            var left = "";

            if (index == 0)
                left = "left:50%;";
            else
                left = "left:0%;";

            if(index === 0)
                prefix_str = "";
            else
                prefix_str = "_next";

            var new_item_selector = "<a href='#'> <div id='" + index + "' class='item' style='position:absolute;top:340px;" + left + "margin-left:-42px;margin-top:-42px;width:84px;height:84px;background-image: url(img/buttons/nr" + (index + 1) + prefix_str + ".png);' ></div> </a>";

            that.append(new_item_selector);

        });

        var first_elem_pos = init_pos - $(document).outerWidth() / 2 + 400;
        var pos = first_elem_pos;

        $(".item").click(function(){
            var id = $(this).attr('id');
            $(this).css("background-image","url(img/buttons/nr" + (parseInt(id) + 1) + ".png)");
            $(this).animate({'left':'50%'}, 1250);

            $(".item").each(function(index, value){
                var that = $(this);
                if (index < parseInt(id)){
                    that.animate({'left':'100%'}, 1250);
                    that.css("background-image","url(img/buttons/nr" + (index + 1) + "_back.png)");
                }else if(index > parseInt(id)){
                    that.animate({'left':'0%'}, 1250);
                    that.css("background-image","url(img/buttons/nr" + (index + 1) + "_next.png)");
                }
            });

            if(id!=="0"){
                pos = first_elem_pos + (settings.page_width * (parseInt(id)) + settings.slide_offset * (parseInt(id)));
            } else {
                pos = first_elem_pos;
            }

            perculator.animate({
                scrollLeft:pos
            },1250);
        });

        $(this).animate({                                   //Initial animation
            scrollLeft:init_pos - ($(document).outerWidth() / 2) + (settings.page_width / 2)
        },1250);

        $('#t0').queue(function (next) {                    //Initial, show first title
            $(this).css({'opacity': 1}, 'slow');
            next();
        });

        $('#0').click(function() {                          //change opacity to active page and rest opacity to others
            $('#t0').animate({'opacity': 1}, 'slow');
            $('#t1').animate({'opacity': 0}, 'slow');
            $('#t2').animate({'opacity': 0}, 'slow');
        });

        $('#1').click(function() {                          //change opacity to active page and rest opacity to others
            $('#t0').animate({'opacity': 0}, 'slow');
            $('#t1').animate({'opacity': 1}, 'slow');
            $('#t2').animate({'opacity': 0}, 'slow');
        });

        $('#2').click(function() {                          //change opacity to active page and rest opacity to others
            $('#t0').animate({'opacity': 0}, 'slow');
            $('#t1').animate({'opacity': 0}, 'slow');
            $('#t2').animate({'opacity': 1}, 'slow');
        });

        $('#clear').click(function() {                          //appears-disappears pages layer

        });

        /*
         $('#fullscreen').click(function () {
         if ($("#pages").is(":hidden")) {
         $(".title").show().animate({left: "50%"}, 700);
         $("#pages").css("left", "");
         $("#pages").show();
         $("#pages").velocity({
         scrollLeft: pos
         }, 1500);
         } else {
         $(".title").animate({left: "-200%"}, 700, function () {
         $(this).hide();
         });
         $("#pages").animate({left: "200%"}, 700, function () {
         $(this).hide();
         });
         //$("#pages").hide();
         }
         });
         */

        return this;
    };
}(jQuery));