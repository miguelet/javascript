<?php
	function applyFilter($text) {
		$patron      = array(
					"/<(\w+)>/",		//for replace html tags
					"/<\/(\w+)>/",		//for replace html tags
					"/http:\/\/(\N+)/"	//for replace links
				    );
		$sustitucion = array(
					"&#60;$1&#62;",
					"&#60;&#47;$1&#62;",
					"<a href='http://$1' class='link'>$1</a>"
				    );
		
		$filtered    = preg_replace($patron,$sustitucion,$text);
		
		return $filtered;
	}
		
?>
