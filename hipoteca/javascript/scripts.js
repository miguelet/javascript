/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function pilla_nombre() {
    var nombre;
    return nombre = valida_nom(document.getElementById("nombre").value);
}

function pilla_ape() {
    var apellido;
    return apellido = valida_apellido1(document.getElementById("apellido").value);
}
function pilla_ape2() {
    var apellido2;
    return apellido2 = valida_apellido2(document.getElementById("apellido2").value);
}
function pilla_edad() {
    var edad;
    return edad = document.getElementById("edad").value
}
function pilla_tlf() {
    var tlf ;
    return tlf = document.getElementById("tlf").value;
    return tlf=funcion_pilladatos_gen(tlf);
    
}

// en proyeccio
function funcion_pilladatos_gen(dato){
    
    var pilla = document.getElementById(dato).value;
    return pilla;
    
}


function pilla_email() {
    var email;
    return email = document.getElementById("email").value;
}
function pilla_interes_fijo() {
    var interes;
    return interes = document.getElementById("interes_fijo").value;
}

function pilla_capital() {
    var capital;
    return capital = document.getElementById("capital").value;
}
function pilla_anyos() {
    var anys;
    var anyos = document.getElementById("Plazo_anyos").value;
    return anyos * 12;
}
function pilla_difrencial() {
    var diferencial = parseFloat(document.getElementById("diferencial").value);
    return diferencial;
}

function pilla_mensuales() {
    var mensuales;
    return parseInt(mensuales = document.getElementById("mensuales").value);
}
function pilla_euribor() {
    var euribor = 0;
    euribor = parseFloat(document.getElementById("euribor").value);
    return euribor;
}
function disable_camps() {
    // funcio  que carrega cuan acomensa o apretem f5
    document.getElementById("cuota_mensual").disabled = true;
    document.getElementById("euribor").disabled = true
    document.getElementById("interes_aplicado").disabled = true;

    document.getElementById("tipo_euri").disabled = true;
    document.getElementById("interes_fijo").disabled = true;
    document.getElementById("diferencial").disabled = true;
    document.getElementById("tipo_euri").selectedIndex = 0;
    document.getElementById("interes").selectedIndex = 0;
}
function enable_camps() {
    // esta funcio habilita els camps o desabilita depenen del ejer
    if (document.getElementById("interes").selectedIndex === 0) {
        document.getElementById("euribor").disabled = true;
        document.getElementById("euribor").value = "";
        document.getElementById("tipo_euri").disabled = true;
        document.getElementById("interes_fijo").disabled = true;
        document.getElementById("interes_fijo").value = "";
        document.getElementById("diferencial").disabled = true;
        document.getElementById("diferencial").value = "";
        document.getElementById("tipo_euri").selectedIndex = 0;
    } else if (document.getElementById("interes").selectedIndex === 1) {
        document.getElementById("euribor").disabled = true;
        document.getElementById("tipo_euri").disabled = true;
        document.getElementById("interes_fijo").disabled = false;
        document.getElementById("diferencial").disabled = true;
        document.getElementById("tipo_euri").selectedIndex = 0;
        document.getElementById("euribor").value = "";
        document.getElementById("diferencial").value = "";
    } else {
        // no vaciar euribor
        document.getElementById("euribor").disabled = true;
        document.getElementById("tipo_euri").disabled = false;
        document.getElementById("interes_fijo").disabled = true;
        document.getElementById("diferencial").disabled = false;
        document.getElementById("interes_fijo").value = "";
    }
}
function euribor_js() {

    if (document.getElementById("tipo_euri").selectedIndex === 0) {

        document.getElementById("euribor").value = "0";


    } else if (document.getElementById("tipo_euri").selectedIndex === 1) {

        document.getElementById("euribor").value = "0.8";

    } else {


        document.getElementById("euribor").value = "0.4";
    }
}
function valida_campos() {
    var ok = true;
    var nombre = pilla_nombre();
    var ape = pilla_ape();
    var ape2 = pilla_ape2();
    var edad = pilla_edad();
    var tlf = pilla_tlf();
    var emai = pilla_email();
    var capital = pilla_capital();
 
    var anys=pilla_anyos();
    if (nombre === false) {
        document.getElementById("nombre").value = "nombre incorrecto";
        ok = false;
    }

    if (ape === false) {
        document.getElementById("apellido").value = "apellido incorrecto";
        ok = false;
    }
    if (ape2 === false) {
        document.getElementById("apellido2").value = "apellido2 incorrecto";
        ok = false;
    }
    if (isNaN(capital)  ){
         document.getElementById("capital").value = "capital incorrecto";
              document.getElementById("cuota_mensual").value = "";
            document.getElementById("interes_aplicado").value = "";
        ok = false;  
    }
  
     if (isNaN(anys)  ){
         document.getElementById("Plazo_anyos").value = "Plazo_anyos incorrecto";
              document.getElementById("cuota_mensual").value = "";
            document.getElementById("interes_aplicado").value = "";
        ok = false;  
    }
    
    return ok;
}
function valida_nom(nom) {
    var valida_nom = /^\D\w{3,}$/;
    return valida_nom.test(nom);
}
function valida_apellido1(apellido1) {
    var valida_apellido1 = /^\D\w{2,}$/;
    return valida_apellido1.test(apellido1);
}
function valida_apellido2(apellido2) {
    var valida_apellido2 = /^\D\w{2,}$/;
    return valida_apellido2.test(apellido2);
}
function pilla_interes_variable() {
    var diferencial = pilla_difrencial();
    var euribor = pilla_euribor();
    var interes_variable = euribor + diferencial;
    return  interes_variable;
}
function  calcular() {
    var descuento = descuento_final();
    var capital = parseFloat(pilla_capital());
    var meses = pilla_anyos();
    var hipoteca = 0;

    if (document.getElementById("interes").selectedIndex === 1) {
        var interes_fijo = pilla_interes_fijo();
        var des_interes_fijo = interes_fijo - descuento;

        //var interes = parseFloat(capital) * parseFloat(interes_fijo1)
        //var interes_capital = parseFloat(capital) + parseFloat(interes);
        //resultado = interes_capital / meses;
        var hipoteca = ((capital * des_interes_fijo) / 12) / (100 * (1 - (Math.pow((1 + ((des_interes_fijo / 12) / 100)), -(meses)))));
        hipoteca = hipoteca.toFixed(2);
        return hipoteca;
    } else if (document.getElementById("interes").selectedIndex === 2) {
        var interes_variable = pilla_interes_variable();
        var des_interes_variable = interes_variable - descuento;

        //   var interes = capital * des_interes_variable
        //  var interes_capital = capital + interes;
        // resultado = interes_capital / meses;


        var hipoteca = ((capital * des_interes_variable) / 12) / (100 * (1 - (Math.pow((1 + ((des_interes_variable / 12) / 100)), -(meses)))));
        hipoteca = hipoteca.toFixed(2);
        return hipoteca;

    }
    return hipoteca;
}
function descuento_final() {
    var descuento = 0;
    var des_casa = descuento_casa();
    var des_vida = descuento_vida();
    var des_nomina = descuento_nomina();
    if (des_casa === true) {
        descuento += 0.5
    }
    if (des_vida === true) {
        descuento += 0.5
    }

    if (des_nomina === true) {
        descuento += 0.5
    }
    return descuento;

}
function antes_pintar() {
    var mensuales1 = parseInt(pilla_mensuales());
    var resultado = parseFloat(calcular());
    var ok = true;
    //sueldo  * 0.4 es major o igual al couta mensal 


    var resultado_mensual = resultado * 0.4
    if (parseFloat(mensuales1) >= resultado_mensual) {
        ok = false;


    }
    return ok;

}
function descuento_casa() {
    var ok;
    if (document.getElementById("seguro_casa").checked == true)
        return ok = true;
    else
        return ok = false;
}
function descuento_nomina() {
    var ok;
    if (document.getElementById("nomina").checked == true)
        return ok = true;
    else
        return ok = false;
}
function descuento_vida() {
    var ok;
    if (document.getElementById("seguro_vida").checked == true)
        return ok = true;
    else
        return ok = false;
}
function pintar_calcular() {
    var resultado = calcular();
    var ok = valida_campos();
    var descuento = descuento_final();
    var sino = antes_pintar();
    if (ok === true) {
        if (sino === false) {
            if (document.getElementById("interes").selectedIndex === 1) {
                var interes_fijo = pilla_interes_fijo();
                var des_interes_fijo = interes_fijo - descuento;
                document.getElementById("cuota_mensual").value = resultado;
                document.getElementById("interes_aplicado").value = des_interes_fijo;

            } else if (document.getElementById("interes").selectedIndex === 2) {
                var interes_variable = pilla_interes_variable();
                var des_interes_variable = interes_variable - descuento;
                document.getElementById("cuota_mensual").value = resultado;
                document.getElementById("interes_aplicado").value = des_interes_variable;
            }
        } else {

            alert("pobres no gracias");
            document.getElementById("cuota_mensual").value = "";
            document.getElementById("interes_aplicado").value = "";
        }
    }


}

function vaciarCampos(campo) {
    document.getElementById(campo).value = "";
    document.getElementById(campo).style.color = 'black';
}