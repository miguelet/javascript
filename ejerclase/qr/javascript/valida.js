
//validem per expresio regular
function is_valid_isbn(isb) {
    var valida_isb = /(?=.{13}$)\d{1,5}([- ])\d{1,7}\1\d{1,6}\1(\d|X)$/i;
    return valida_isb.test(isb);

}

// validem que siga un isbn valid
function valido_existente_isbn(isbn) {
    if (isbn.length == 10) {
        var acum = 0;
        for (var i = 1; i < isbn.length; i++) {
            acum += i * isbn[i - 1];
        }
        if (acum % 11 < 10 && acum % 11 == isbn[isbn.length - 1]) {
            return true;
        } else if (acum % 11 == 10 && "X" == isbn[isbn.length - 1].toUpperCase()) {
            return true;
        } else {
            return false;
        }
    } else if (isbn.length == 13) {
        var acum = 0;
        var initial = 1;
        for (var i = 1; i < isbn.length; i++) {
            acum += initial * isbn[i - 1];
            initial == 1 ? initial = 3 : initial = 1;
        }
        if ((acum % 10 == 0 && isbn[isbn.length - 1] == 0) || (10 - acum % 10) == isbn[isbn.length - 1]) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}


